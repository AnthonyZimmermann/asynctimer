import asyncio

class AsyncTimer(object):
    def __init__(self, coroutine, delay=0, loop=None, name=None):
        self.coroutine = coroutine
        self.delay = delay
        self.loop = loop
        self.name = name

        self.delayed_coroutine = None
        self.task = None

    def __del__(self):
        self.cancel()

    def run(self, delay=None, loop=None):
        self.delay = delay if delay is not None else self.delay
        self.delayed_coroutine = AsyncTimer.createCoroutine(self.coroutine,self.delay)

        if loop is not None:
            self.loop = loop
        if self.loop is None:
            self.loop = asyncio.get_event_loop()

        self.task = self.loop.create_task(self.delayed_coroutine, name=self.name)
        return self

    def cancel(self):
        if self.task is not None:
            self.task.cancel()
        if self.coroutine is not None:
            self.coroutine.close()
        if self.delayed_coroutine is not None:
            self.delayed_coroutine.close()

    def started(self):
        return self.task is not None

    def running(self):
        return self.task is not None and self.task.done() is False

    def done(self):
        return self.task is not None and self.task.done()

    async def getResult(self, catch_exception=True):
        try:
            await self.task
            return self.task.result()
        except:
            if catch_exception:
                return None
            else:
                raise

    @staticmethod
    def createCoroutine(coroutine, delay):
        async def delayed_coroutine():
            await asyncio.sleep(delay)
            return await coroutine
        return delayed_coroutine()

    @staticmethod
    def createTask(coroutine, delay, loop=None, name=None):
        coroutine = AsyncTimer.createCoroutine(coroutine,delay)
        if loop is None:
            loop = asyncio.get_event_loop()
        return loop.create_task(coroutine, name=name)
