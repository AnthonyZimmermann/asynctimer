import asyncio
import time
import unittest
import warnings

from azutils.asynctimer.asynctimer import AsyncTimer

class TestAsyncTimerConstructor(unittest.TestCase):

    async def dummyAsync(self, ret=None):
        asyncio.sleep(0.1)
        return ret

    def test_minimal_constructor(self):
        coroutine = self.dummyAsync()
        timer = AsyncTimer(coroutine, delay=0.1)
        assert isinstance(timer, AsyncTimer)
        assert timer.coroutine == coroutine
        assert asyncio.iscoroutine(timer.coroutine)

    def test_event_loop(self):
        loop = asyncio.get_event_loop()
        timer = AsyncTimer(self.dummyAsync(), delay=0.2, loop=loop)
        assert timer.loop == loop

    def test_name(self):
        name = "timer_name"
        timer = AsyncTimer(self.dummyAsync(), 0.3, name=name)
        assert timer.name == name

class TestAsyncTimerExecution(object):
    DUMMY_ASYNC_SLEEP_TIME = 0.01

    def run_all_tasks(self, loop=None, tasks=None):
        if loop is None:
            loop = asyncio.get_event_loop()
        if tasks is None:
            tasks = asyncio.all_tasks(loop)
        return loop.run_until_complete(asyncio.gather(*tasks))


    async def dummyAsync(self, ret):
        await asyncio.sleep(TestAsyncTimerExecution.DUMMY_ASYNC_SLEEP_TIME)
        return ret

    def test_instace_run(self):
        delay_time = 0.1
        runtime = delay_time + TestAsyncTimerExecution.DUMMY_ASYNC_SLEEP_TIME
        t0 = time.time()
        timer = AsyncTimer(self.dummyAsync("done"), delay=delay_time).run()
        loop = asyncio.get_event_loop()

        async def check():
            result = await timer.task
            tdiff = time.time()-t0
            assert 0.98*runtime < tdiff < 1.02*runtime

        check_task = loop.create_task(check())

        val,_ = self.run_all_tasks(loop, [timer.task,check_task])
        assert val == "done"
        assert loop.run_until_complete(timer.getResult()) == "done"

    def test_instance_cancel_wo_exception(self):
        timer = AsyncTimer(self.dummyAsync("jo"), delay=0.2).run()
        loop = asyncio.get_event_loop()
        async def cancel(t):
            await asyncio.sleep(t)
            timer.cancel()
        async def dummyWait():
            await asyncio.sleep(0.3)
        loop.create_task(cancel(0.1))
        loop.run_until_complete(dummyWait())
        res = loop.run_until_complete(timer.getResult())
        assert res == None

    def test_instance_cancel_w_exception(self):
        timer = AsyncTimer(self.dummyAsync("jo"), delay=0.2).run()
        loop = asyncio.get_event_loop()
        async def cancel(t):
            await asyncio.sleep(t)
            timer.cancel()
        async def dummyWait():
            await asyncio.sleep(0.3)
        loop.create_task(cancel(0.1))
        loop.run_until_complete(dummyWait())
        exception_catched = False
        try:
            loop.run_until_complete(timer.getResult(catch_exception=False))
        except:
            exception_catched = True
        assert exception_catched

    def test_static_coroutine(self):
        coroutine = AsyncTimer.createCoroutine(self.dummyAsync("dummy"), delay=0.1)
        loop = asyncio.get_event_loop()
        t0 = time.time()
        res = loop.run_until_complete(coroutine)
        t1 = time.time()
        assert 0.09 < t1-t0 < 1.01
        assert res == "dummy"

    def test_static_task(self):
        task = AsyncTimer.createTask(self.dummyAsync("foo"), delay=0.1)
        loop = asyncio.get_event_loop()
        t0 = time.time()
        res = loop.run_until_complete(task)
        t1 = time.time()
        assert 0.09 < t1-t0 < 1.01
        assert res == "foo"
